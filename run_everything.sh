export SNPsinExons="../SNPs_in_nonExon.bed"
export allSNPsinExons="../allSNPs_in_nonExon.bed"
export SankSNPsinExons="../../Sankaran_SNPs/SNPs_in_nonExon.bed"
export collapsedNonExonEnhs="../../MACS/D0H6D4D5_k27ATACK27_nonExon_merge.bed"
export sigCtr="../../MACS/D0H6D4D5_k27ATACK27_nonExon_wIntPeak.merge.bed" 
export gataOnly="../../MACS/D0H6D4D5_k27ATACK27_nonExon.wGATAnoSMAD.merge.bed"
export nonSigCtrEnh="../../MACS/D0H6D4D5_k27ATACK27_nonExon_merge.woIntPeak.bed"


rm shufSNPs_in_sigCtr.txt shufAllSNPs_in_sigCtr.txt shufSNPs_in_GATAonly.txt shufAllSNPs_in_GATAonly.txt
for i in {1..10000}
do
#   bedtools shuffle -incl $collapsedNonExonEnhs -i $SNPsinExons -g /nfs/genomes/human_gp_feb_09_no_random/anno/chromInfo.txt > RandSNPs/SNPs_in_nonExon.$i.bed
  export SNPs_in_sigCtr=$(intersectBed -u -a RandSNPs/SNPs_in_nonExon.$i.bed -b $sigCtr | wc -l)
  echo -e "$i\t$SNPs_in_sigCtr" >> shufSNPs_in_sigCtr.txt

  export SNPs_in_GATA=$(intersectBed -u -a RandSNPs/SNPs_in_nonExon.$i.bed -b $gataOnly | wc -l)
  echo -e "$i\t$SNPs_in_GATA" >> shufSNPs_in_GATAonly.txt
####

#   bedtools shuffle -incl $collapsedNonExonEnhs -i $allSNPsinExons -g /nfs/genomes/human_gp_feb_09_no_random/anno/chromInfo.txt > allRandSNPs/allSNPs_in_nonExon.$i.bed
  export allSNPs_in_sigCtr=$(intersectBed -u -a allRandSNPs/allSNPs_in_nonExon.$i.bed -b $sigCtr | wc -l)
  echo -e "$i\t$allSNPs_in_sigCtr" >> shufAllSNPs_in_sigCtr.txt

  export SNPs_in_GATA=$(intersectBed -u -a allRandSNPs/allSNPs_in_nonExon.$i.bed -b $gataOnly | wc -l)
  echo -e "$i\t$allSNPs_in_GATA" >> shufAllSNPs_in_GATAonly.txt

####
#   bedtools shuffle -incl $collapsedNonExonEnhs -i $SankSNPsinExons -g /nfs/genomes/human_gp_feb_09_no_random/anno/chromInfo.txt > SankRandSNPs/SankSNPs_in_nonExon.$i.bed
  export SankSNPs_in_sigCtr=$(intersectBed -u -a SankRandSNPs/SankSNPs_in_nonExon.$i.bed -b $sigCtr | wc -l)
  echo -e "$i\t$SankSNPs_in_sigCtr" >> shufSankSNPs_in_sigCtr.txt

  export SNPs_in_GATA=$(intersectBed -u -a SankRandSNPs/SankSNPs_in_nonExon.$i.bed -b $gataOnly | wc -l)
  echo -e "$i\t$SankSNPs_in_GATA" >> shufSankSNPs_in_GATAonly.txt

done


intersectBed -u -a $SNPsinExons -b $sigCtr | wc -l  # 124
sort -k2,2n shufSNPs_in_sigCtr.txt | tail  # 90
intersectBed -u -a $allSNPsinExons -b $sigCtr | wc -l  # 873
sort -k2,2n shufAllSNPs_in_sigCtr.txt | tail  # 680
intersectBed -u -a $SankSNPsinExons -b $sigCtr | wc -l  # 1252   #1160 unique SNP ids
intersectBed -u -a $SankSNPsinExons -b $sigCtr | awk -F\\t '{print $4}' | awk -F'_' '{print $1}' | sort | uniq | wc -l # 1160
sort -k2,2n shufSankSNPs_in_sigCtr.txt | tail  # 1104


intersectBed -u -a $SNPsinExons -b $gataOnly | wc -l  # 43
sort -k2,2n shufSNPs_in_GATAonly.txt | head  # 
awk -F\\t '{if($2 < 43) sum+=1} END { print sum}' shufSNPs_in_GATAonly.txt # p=0.9166
intersectBed -u -a $allSNPsinExons -b $gataOnly | wc -l  # 336
sort -k2,2n shufSNPs_in_GATAonly.txt | head  # 
awk -F\\t '{if($2 < 336) sum+=1} END { print sum}' shufAllSNPs_in_GATAonly.txt # p=1
intersectBed -u -a $SankSNPsinExons -b $gataOnly | wc -l  # 584
sort -k2,2n shufSankSNPs_in_GATAonly.txt | head  # 
awk -F\\t '{if($2 < 584) sum+=1} END { print sum}' shufSankSNPs_in_GATAonly.txt # p=1


exit

###### SNPs in sTF MOTIF HITS in TSCs vs. NON


rm shufSNPs_in_sigCtrSTFs.txt shufSNPs_in_nonSigCtrSTFs.txt shufAllSNPs_in_nonSigCtrSTFs.txt shufAllSNPs_in_sigCtrSTFs.txt shufSankSNPs_in_sigCtrSTFs.txt shufSankSNPs_in_nonSigCtrSTFs.txt
for i in {1..10000}
do
#   bedtools shuffle -incl $collapsedNonExonEnhs -i $SNPsinExons -g /nfs/genomes/human_gp_feb_09_no_random/anno/chromInfo.txt > RandSNPs/SNPs_in_nonExon.$i.bed

  intersectBed -u -a RandSNPs/SNPs_in_nonExon.$i.bed -b $sigCtr > theseInSigCtr.bed
  awk -F\\t '{print $4}' theseInSigCtr.bed | grep -w -f - ../total_tight_AAPCombinedMEME.txt2 >| theseInSigCtr.motifs.txt
  export SNPs_in_sigCtr=$(grep -w -f ../../Motifs/190820_AvikCombinedMEME_CISBP_sTF_motifs.txt theseInSigCtr.motifs.txt | grep -v -w -f ../../Motifs/190820_AvikCombinedMEME_CISBP_mTF_motifs.txt | awk -F\\t '{print $1}' | sort | uniq | wc -l) 
  echo -e "$i\t$SNPs_in_sigCtr" >> shufSNPs_in_sigCtrSTFs.txt

  intersectBed -u -a RandSNPs/SNPs_in_nonExon.$i.bed -b $nonSigCtrEnh > theseInNonSigCtr.bed
  awk -F\\t '{print $4}' theseInNonSigCtr.bed | grep -w -f - ../total_tight_AAPCombinedMEME.txt2 >| theseInNonSigCtr.motifs.txt
  export SNPs_in_sigCtr=$(grep -w -f ../../Motifs/190820_AvikCombinedMEME_CISBP_sTF_motifs.txt theseInNonSigCtr.motifs.txt | grep -v -w -f ../../Motifs/190820_AvikCombinedMEME_CISBP_mTF_motifs.txt | awk -F\\t '{print $1}' | sort | uniq | wc -l) 
  echo -e "$i\t$SNPs_in_sigCtr" >> shufSNPs_in_nonSigCtrSTFs.txt
####

  intersectBed -u -a allRandSNPs/allSNPs_in_nonExon.$i.bed -b $sigCtr > theseInSigCtr.bed
  awk -F\\t '{print $4}' theseInSigCtr.bed | grep -w -f - ../total_tight_AAPallCombinedMEME.txt2 >| theseInSigCtr.motifs.txt
  export SNPs_in_sigCtr=$(grep -w -f ../../Motifs/190820_AvikCombinedMEME_CISBP_sTF_motifs.txt theseInSigCtr.motifs.txt | grep -v -w -f ../../Motifs/190820_AvikCombinedMEME_CISBP_mTF_motifs.txt | awk -F\\t '{print $1}' | sort | uniq | wc -l) 
  echo -e "$i\t$SNPs_in_sigCtr" >> shufAllSNPs_in_sigCtrSTFs.txt

  intersectBed -u -a allRandSNPs/allSNPs_in_nonExon.$i.bed -b $nonSigCtrEnh > theseInNonSigCtr.bed
  awk -F\\t '{print $4}' theseInNonSigCtr.bed | grep -w -f - ../total_tight_AAPallCombinedMEME.txt2 >| theseInNonSigCtr.motifs.txt
  export SNPs_in_sigCtr=$(grep -w -f ../../Motifs/190820_AvikCombinedMEME_CISBP_sTF_motifs.txt theseInNonSigCtr.motifs.txt | grep -v -w -f ../../Motifs/190820_AvikCombinedMEME_CISBP_mTF_motifs.txt | awk -F\\t '{print $1}' | sort | uniq | wc -l) 
  echo -e "$i\t$SNPs_in_sigCtr" >> shufAllSNPs_in_nonSigCtrSTFs.txt

####
  intersectBed -u -a SankRandSNPs/SankSNPs_in_nonExon.$i.bed -b $sigCtr > theseInSigCtr.bed
  awk -F\\t '{print $4}' theseInSigCtr.bed | grep -w -f - ../../Sankaran_SNPs/total_tight_AAPCISBP.txt2 >| theseInSigCtr.motifs.txt
  export SNPs_in_sigCtr=$(grep -w -f ../../Motifs/190820_AvikCombinedMEME_CISBP_sTF_motifs.txt theseInSigCtr.motifs.txt | grep -v -w -f ../../Motifs/190820_AvikCombinedMEME_CISBP_mTF_motifs.txt | awk -F\\t '{print $1}' | sort | uniq | wc -l) 
  echo -e "$i\t$SNPs_in_sigCtr" >> shufSankSNPs_in_sigCtrSTFs.txt

  intersectBed -u -a SankRandSNPs/SankSNPs_in_nonExon.$i.bed -b $nonSigCtrEnh > theseInNonSigCtr.bed
  awk -F\\t '{print $4}' theseInNonSigCtr.bed | grep -w -f - ../../Sankaran_SNPs/total_tight_AAPCISBP.txt2 >| theseInNonSigCtr.motifs.txt
  export SNPs_in_sigCtr=$(grep -w -f ../../Motifs/190820_AvikCombinedMEME_CISBP_sTF_motifs.txt theseInNonSigCtr.motifs.txt | grep -v -w -f ../../Motifs/190820_AvikCombinedMEME_CISBP_mTF_motifs.txt | awk -F\\t '{print $1}' | sort | uniq | wc -l) 
  echo -e "$i\t$SNPs_in_sigCtr" >> shufSankSNPs_in_nonSigCtrSTFs.txt


done


sort -k2,2n shufSNPs_in_sigCtrSTFs.txt | tail -n 2  # 47
sort -k2,2n shufAllSNPs_in_sigCtrSTFs.txt | tail -n 2 # 299
sort -k2,2n shufSankSNPs_in_sigCtrSTFs.txt | tail -n 2  # 458


intersectBed -u -a $SNPsinExons -b $sigCtr > realInSigCtr.bed
awk -F\\t '{print $4}' realInSigCtr.bed | grep -w -f - ../total_tight_AAPCombinedMEME.txt2 >| realInSigCtr.motifs.txt
grep -w -f ../../Motifs/190820_AvikCombinedMEME_CISBP_sTF_motifs.txt realInSigCtr.motifs.txt | grep -v -w -f ../../Motifs/190820_AvikCombinedMEME_CISBP_mTF_motifs.txt | awk -F\\t '{print $1}' | sort | uniq | wc -l    # 53

intersectBed -u -a $allSNPsinExonx -b $sigCtr > realAllInSigCtr.bed
awk -F\\t '{print $4}' realAllInSigCtr.bed | grep -w -f - ../total_tight_AAPallCombinedMEME.txt2 >| realAllInSigCtr.motifs.txt
grep -w -f ../../Motifs/190820_AvikCombinedMEME_CISBP_sTF_motifs.txt realAllInSigCtr.motifs.txt | grep -v -w -f ../../Motifs/190820_AvikCombinedMEME_CISBP_mTF_motifs.txt | awk -F\\t '{print $1}' | sort | uniq | wc -l    # 356

intersectBed -u -a $SankSNPsinExons -b $sigCtr > realSankInSigCtr.bed
awk -F\\t '{print $4}' realSankInSigCtr.bed | grep -w -f - ../../Sankaran_SNPs/total_tight_AAPCombinedMEME.txt2 >| realSankInSigCtr.motifs.txt
grep -w -f ../../Motifs/190820_AvikCombinedMEME_CISBP_sTF_motifs.txt realSankInSigCtr.motifs.txt | grep -v -w -f ../../Motifs/190820_AvikCombinedMEME_CISBP_mTF_motifs.txt | awk -F\\t '{print $1}' | sort | uniq | wc -l    # 481

